package com.template.android;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.template.android.R;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

	private DrawerLayout drawer;
	private Toolbar toolbar;
	private ActionBarDrawerToggle drawerToggle;
	private MenuItem currentMenuItem;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set a Toolbar to replace the ActionBar.
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Find our drawer view
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerToggle = setupDrawerToggle();
		// Tie DrawerLayout events to the ActionBarToggle
		drawer.setDrawerListener(drawerToggle);

		// Set the menu icon instead of the launcher icon.
		final ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);

		// Find our drawer view
		NavigationView nvDrawer = (NavigationView) findViewById(R.id.navigation_view);
		// Setup drawer view
		setupDrawerContent(nvDrawer);
}		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);

		SearchView searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// The action bar home/up action should open or close the drawer.
		switch (item.getItemId()) {
			case android.R.id.home:
				drawer.openDrawer(GravityCompat.START);
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		drawerToggle.onConfigurationChanged(newConfig);
	}

	private ActionBarDrawerToggle setupDrawerToggle() {
		return new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
	}

	private void setupDrawerContent(NavigationView navigationView) {
		navigationView.setNavigationItemSelectedListener(
			new NavigationView.OnNavigationItemSelectedListener() {
				@Override
				public boolean onNavigationItemSelected(MenuItem menuItem) {
					selectDrawerItem(menuItem);
					return true;
				}
			}
		);
	}

	public void selectDrawerItem(MenuItem menuItem) {
		Class fragmentClass;
		switch (menuItem.getItemId()) {
			case R.id.action_one:
				fragmentClass = FragmentOne.class;
				break;
			case R.id.action_two:
				fragmentClass = FragmentTwo.class;
				break;
			case R.id.action_settings:
				fragmentClass = FragmentSettings.class;
				break;
			case R.id.action_question_answer:
				fragmentClass = FragmentQuestionAnswer.class;
				break;
			case R.id.action_info_outline:
				fragmentClass = FragmentInfoOutline.class;
				break;
			default:
				fragmentClass =	FragmentQuestionAnswer.class;
		}

		Fragment fragment = null;
		try {
			fragment = (Fragment) fragmentClass.newInstance();
		}
		catch (Exception e) {
			Log.e("MainActivity", "Error while creating Fragment " + fragmentClass, e);
		}

		// Insert the fragment by replacing any existing fragment
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();

		// Highlight the selected item, update the title, and close the drawer
		menuItem.setCheckable(true);
		menuItem.setChecked(true);
		
		// Highlight the selected item, update the title, and close the drawer
		menuItem.setCheckable(true);
		menuItem.setChecked(true);

		// Workaround since checking/unchecking for MenuItems of different groups does not work correctly
		if (currentMenuItem != null && menuItem.getItemId() != currentMenuItem.getItemId()) {
			currentMenuItem.setChecked(false);
		}
		currentMenuItem = menuItem;
		
		setTitle(menuItem.getTitle());
		drawer.closeDrawers();
	}
}
